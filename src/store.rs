//! Storing of objects in S3

use crate::error::Result;
use crate::lo::{Data, Lo};
use rusoto_s3::{
    AbortMultipartUploadRequest, CompleteMultipartUploadRequest, CompletedMultipartUpload,
    CompletedPart, CreateMultipartUploadRequest, PutObjectRequest, S3Client, UploadPartRequest, S3,
};
use std::fs::File;
use std::io::{self, Read};
use std::sync::atomic::{AtomicU64, Ordering};
use std::sync::Arc;
use tokio::runtime::Runtime as TokioRuntime;

impl Lo {
    /// Store Large Object on S3
    ///
    /// Store Large Object data on S3 using the sha2 hash as key. The data in memory or the
    /// temporary file held by [`Data`] is dropped.
    pub fn store(
        &mut self,
        client: &S3Client,
        bucket: &str,
        chunk_size: usize,
        byte_counter: &Arc<AtomicU64>,
        mut rt: &mut TokioRuntime,
    ) -> Result<()> {
        let lo_data = self.take_lo_data();
        match lo_data {
            Data::File(ref temp) => {
                let mut file = File::open(&temp.path())?;
                if self.size() <= chunk_size as i64 {
                    #[allow(clippy::cast_sign_loss, clippy::cast_possible_truncation)]
                    let mut data = Vec::with_capacity(self.size() as usize);
                    file.read_to_end(&mut data)?;
                    assert_eq!(
                        self.size(),
                        data.len() as i64,
                        "unexpected size ({}) of buffer file for {:?}",
                        data.len(),
                        self
                    );
                    self.upload(data, client, bucket, byte_counter, &mut rt)
                } else {
                    self.upload_multipart(
                        &mut file,
                        client,
                        bucket,
                        chunk_size,
                        byte_counter,
                        &mut rt,
                    )
                }
            }
            Data::Vector(data) => self.upload(data, client, bucket, byte_counter, &mut rt),
            Data::None => panic!("Large Object must be fetched first"),
        }
    }

    fn upload(
        &self,
        data: Vec<u8>,
        client: &S3Client,
        bucket: &str,
        byte_counter: &Arc<AtomicU64>,
        rt: &mut TokioRuntime,
    ) -> Result<()> {
        let len = data.len();
        let request = PutObjectRequest {
            key: self.sha2_hex().expect("Large Object must be fetched first"),
            bucket: bucket.to_string(),
            body: Some(data.into()),
            content_type: Some(self.mime_type().to_string()),
            ..Default::default()
        };
        rt.block_on(client.put_object(request))?;
        byte_counter.fetch_add(len as u64, Ordering::Relaxed);
        Ok(())
    }

    fn upload_multipart<R>(
        &self,
        data: &mut R,
        client: &S3Client,
        bucket: &str,
        chunk_size: usize,
        byte_counter: &Arc<AtomicU64>,
        mut rt: &mut TokioRuntime,
    ) -> Result<()>
    where
        R: Read,
    {
        let key = self.sha2_hex().expect("Large Object must be fetched first");
        let upload = rt.block_on(
            client.create_multipart_upload(CreateMultipartUploadRequest {
                key: key.clone(),
                bucket: bucket.to_string(),
                content_type: Some(self.mime_type().to_string()),
                ..Default::default()
            }),
        )?;

        let upload_id = upload.upload_id.expect("Missing upload ID");
        let mut buffer = vec![0; chunk_size];
        let mut tot_len: i64 = 0;
        let mut parts = Vec::new();
        'outer: for part in 1.. {
            loop {
                match data.read(&mut buffer) {
                    Ok(0) => break 'outer,
                    Ok(len) => {
                        tot_len += len as i64;
                        let part = self.upload_part(
                            client,
                            bucket,
                            &key,
                            &upload_id,
                            part,
                            &buffer[..len],
                            &mut rt,
                        )?;
                        parts.push(part);
                        byte_counter.fetch_add(len as u64, Ordering::Relaxed);
                        break;
                    }
                    Err(ref e) if e.kind() == io::ErrorKind::Interrupted => {
                        debug!("Interrupt while reading from buffer file of {:?}", self);
                    }
                    Err(e) => return Err(e.into()),
                }
            }
        }

        assert_eq!(
            self.size(),
            tot_len,
            "Unexpected size ({}) of buffer file for {:?}",
            tot_len,
            self
        );

        rt.block_on(
            client.complete_multipart_upload(CompleteMultipartUploadRequest {
                bucket: bucket.to_owned(),
                key,
                upload_id,
                multipart_upload: Some(CompletedMultipartUpload { parts: Some(parts) }),
                ..Default::default()
            }),
        )?;

        Ok(())
    }

    fn upload_part(
        &self,
        client: &S3Client,
        bucket: &str,
        key: &str,
        upload_id: &str,
        part: i64,
        data: &[u8],
        mut rt: &mut TokioRuntime,
    ) -> Result<CompletedPart> {
        let resp = rt.block_on(client.upload_part(UploadPartRequest {
            bucket: bucket.to_string(),
            key: key.to_owned(),
            upload_id: upload_id.to_owned(),
            part_number: part,
            body: Some(data.to_vec().into()),
            ..Default::default()
        }));

        match resp {
            Ok(r) => Ok(CompletedPart {
                e_tag: r.e_tag,
                part_number: Some(part),
            }),
            Err(e) => {
                self.abort_upload(client, bucket, &key, &upload_id, &mut rt);
                Err(e.into())
            }
        }
    }

    fn abort_upload(
        &self,
        client: &S3Client,
        bucket: &str,
        key: &str,
        upload_id: &str,
        rt: &mut TokioRuntime,
    ) {
        let status = rt.block_on(client.abort_multipart_upload(AbortMultipartUploadRequest {
            bucket: bucket.to_owned(),
            key: key.to_owned(),
            upload_id: upload_id.to_owned(),
            ..Default::default()
        }));

        if let Err(e) = status {
            error!("failed to abort multipart upload for {:?}: {}", self, e);
        }
    }
}
