//! Commit sha2 hashes to the database
//!
//! used by committer thread

use crate::error::Result;
use crate::lo::Lo;
use postgres::Connection;
use std::sync::atomic::{AtomicU64, Ordering};
use std::sync::Arc;

/// Commit the sha2 hashes of the given [`Lo`]s to database.
pub fn commit(conn: &Connection, objects: &[Lo], byte_counter: &Arc<AtomicU64>) -> Result<()> {
    let stmt = conn.prepare_cached("UPDATE _nice_binary SET sha2 = $1 WHERE hash = $2")?;
    let tx = conn.transaction()?;
    let mut bytes = 0_u64;

    for lo in objects {
        trace!("preparing to commit new hash for large object: {:?}", lo);
        let sha2 = lo.sha2_hex().expect("SHA2 hash unknown");
        if stmt.execute(&[&sha2, &lo.sha1_hex()])? == 0 {
            warn!(
                "could not update sha2 hash for lo (did it vanish?): {:?}",
                &lo
            );
        }
        #[allow(clippy::cast_sign_loss)]
        let size = lo.size() as u64;
        bytes += size;
    }

    tx.commit()?;
    byte_counter.fetch_add(bytes, Ordering::Relaxed);
    Ok(())
}
