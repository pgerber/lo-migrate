//! counter thread implementation
//!
//! Count the number of Large Objects.

use crate::error::Result;
use crate::thread::ThreadStat;
use postgres::Connection;

pub struct Counter<'a> {
    stats: &'a ThreadStat,
    conn: &'a Connection,
}

struct Stats {
    remaining_objects: i64,
    total_objects: i64,
    remaining_size: i64,
    total_size: i64,
}

impl<'a> Counter<'a> {
    pub fn new(stats: &'a ThreadStat, conn: &'a Connection) -> Self {
        Counter { stats, conn }
    }

    #[allow(clippy::cast_sign_loss)]
    pub fn start_worker(&self) -> Result<()> {
        let stats = self.get_stats()?;
        *self
            .stats
            .lo_remaining
            .lock()
            .expect("failed to acquire lock") = Some(stats.remaining_objects as u64);
        *self.stats.lo_total.lock().expect("failed to acquire lock") =
            Some(stats.total_objects as u64);
        *self
            .stats
            .bytes_remaining
            .lock()
            .expect("failed to acquire lock") = Some(stats.remaining_size as u64);
        *self
            .stats
            .bytes_total
            .lock()
            .expect("failed to acquire lock") = Some(stats.total_size as u64);
        info!("thread has completed its mission");
        Ok(())
    }

    /// count large object in database that still need to be moved to S3
    ///
    /// note: we pass in the transaction to be sure that the count is correct; Count must occur in
    ///       same transaction as retrieving the rows to be correct.
    fn get_stats(&self) -> Result<Stats> {
        info!("counting large objects");

        let has_history = !self
            .conn
            .query(
                "SELECT 1 FROM information_schema.tables WHERE table_name = 'nice_history_domain_entity'",
                &[],
            )
            .expect("failed to get schema information")
            .is_empty();
        let condition = if has_history {
            "AND NOT EXISTS ( \n\
             SELECT * FROM nice_history_domain_entity WHERE \"xmlContent\" = hash \n\
             )"
        } else {
            ""
        };

        // Entries starting with beefbeefbeef... are created during the history migration. It's used as placeholder
        // to allow running the history and S3 migration at the same time.
        let rows = self.conn.query(
            &format!(
                r#"
                    SELECT
                        (SELECT count(*) FROM _nice_binary
                            WHERE sha2 IS NULL {0}),
                        (SELECT count(*) FROM _nice_binary
                            WHERE (sha2 IS NULL
                                OR sha2 NOT LIKE 'beefbeefbeefbeefbeefbeefbeefbeefbeefbeefbeefbeefbeef%') {0}),
                        (SELECT coalesce(sum(size), 0)::bigint FROM _nice_binary
                            WHERE sha2 is NULL {0}),
                        (SELECT coalesce(sum(size), 0)::bigint FROM _nice_binary
                            WHERE (sha2 IS NULL
                                OR sha2 NOT LIKE 'beefbeefbeefbeefbeefbeefbeefbeefbeefbeefbeefbeefbeef%') {0})
                "#,
                condition
            ),
            &[],
        )?;
        let row = rows.get(0);

        Ok(Stats {
            remaining_objects: row.get(0),
            total_objects: row.get(1),
            remaining_size: row.get(2),
            total_size: row.get(3),
        })
    }
}
