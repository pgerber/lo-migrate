//! observer thread implementation
//!
//! The observer thread retrieves the list of Large Objects and passes them to the receiver thread.

use crate::serialize::hex::FromHex;
use fallible_iterator::FallibleIterator;
use postgres::rows::Row;
use postgres::types::Oid;
use postgres::Connection;
use std::sync::atomic::Ordering;
use std::sync::Arc;
use two_lock_queue::Sender;

use super::*;
use crate::error::Result;

pub struct Observer<'a> {
    stats: &'a ThreadStat,
    conn: &'a Connection,
}

impl<'a> Observer<'a> {
    pub fn new(thread_stat: &'a ThreadStat, conn: &'a Connection) -> Self {
        Observer {
            stats: thread_stat,
            conn,
        }
    }

    pub fn start_worker(&self, tx: Arc<Sender<Lo>>, buffer_size: i32) -> Result<()> {
        let trx = self.conn.transaction()?;

        let has_history = !self
            .conn
            .query(
                "SELECT 1 FROM information_schema.tables WHERE table_name = 'nice_history_domain_entity'",
                &[],
            )
            .expect("failed to get schema information")
            .is_empty();
        let sql = if has_history {
            "SELECT hash, data, size, mime_type FROM _nice_binary \n\
             WHERE sha2 is NULL \n\
             AND NOT EXISTS ( \n\
             SELECT * FROM nice_history_domain_entity WHERE \"xmlContent\" = hash \n\
             )"
        } else {
            "SELECT hash, data, size, mime_type FROM _nice_binary WHERE sha2 is NULL"
        };

        let stmt = self.conn.prepare(sql)?;
        let rows = stmt.lazy_query(&trx, &[], buffer_size)?;
        for row in rows.iterator() {
            self.queue(&tx, row?)?;

            // thread cancellation point
            self.stats.cancellation_point()?;
        }

        info!("thread has completed its mission");
        Ok(())
    }

    /// add [`Lo`] to receiver queue
    fn queue(&self, tx: &Sender<Lo>, row: Row) -> Result<()> {
        let sha1_hex: String = row.get(0);
        let sha1 = sha1_hex.from_hex();
        let oid: Oid = row.get(1);
        let size: i64 = row.get(2);
        let mime_type: String = row.get(3);

        match sha1 {
            Ok(ref sha1) if sha1.len() != 20 => {
                error!(
                    "encountered _nice_binary entry with invalid hash {:?}: incorrect length",
                    sha1_hex
                );
                self.stats.lo_failed.fetch_add(1, Ordering::Relaxed);
            }
            Err(e) => {
                error!(
                    "encountered _nice_binary entry with invalid hash {:?}: {}",
                    sha1_hex, e
                );
                self.stats.lo_failed.fetch_add(1, Ordering::Relaxed);
            }
            Ok(sha1) => {
                let lo = Lo::new(sha1, oid, size, mime_type);
                trace!("adding Lo to queue: {:?}", lo);
                tx.send(lo)?;

                // count received objects
                self.stats.lo_observed.fetch_add(1, Ordering::Relaxed);
            }
        }
        Ok(())
    }
}
