//! Collections of small utility functions

use postgres::Connection;

const BATCH_NAME: &str = "nice2.dms.DeleteUnreferencedBinariesBatchJob";

/// Ensure Nice's `DeleteUnreferencedBinariesBatchJob` is no longer active
///
/// An error is returned if the batch job is still active or if it doesn't exists.
pub fn check_batch_job_is_disabled(conn: &Connection) -> Result<(), String> {
    let rows = conn
        .query(
            "SELECT active FROM nice_batch_job WHERE id = $1",
            &[&BATCH_NAME],
        )
        .expect("SQL query failed");

    match rows.len() {
        0 => {
            info!("Batch job {:?} not found, ignoring …", BATCH_NAME);
            Ok(())
        }
        1 => {
            let active: bool = rows.get(0).get(0);
            if active {
                Err(format!(
                    "Batch job {:?} must be deactivated before the migration can be \
                     started",
                    BATCH_NAME
                ))
            } else {
                Ok(())
            }
        }
        _ => unreachable!("query returned {} rows instead of zero or one", rows.len()),
    }
}

/// Check if migration has been completed by checking if _nice_binary.hash
/// contains sha2 hashes.
pub fn has_migration_been_completed(conn: &Connection) -> bool {
    let hash: String = conn
        .query("SELECT hash FROM _nice_binary FETCH FIRST 1 ROW ONLY", &[])
        .expect("SQL query failed")
        .get(0)
        .get(0);

    assert!(
        hash.chars().all(|c| matches!(c, '0'..='9' | 'a'..='f')),
        "Hash of unknown type encountered: {:?}",
        hash
    );

    match hash.len() {
        64 => true,
        40 => false,
        _ => panic!("Hash of unknown type encountered: {:?}", hash),
    }
}
