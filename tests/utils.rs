#![cfg(feature = "postgres_tests")]
extern crate lo_migrate;
extern crate postgres;

mod common;
use crate::common::*;

#[test]
fn migration_complete() {
    let conn = postgres_conn();
    create_table_with_hash(
        &conn,
        "1a39259cc3ff086fae14e7ce7291a7ffac9b33f784a32fb59c4b9b4255d5812a",
    );
    assert!(lo_migrate::utils::has_migration_been_completed(&conn));
}

#[test]
fn migration_incomplete() {
    let conn = postgres_conn();
    create_table_with_hash(&conn, "b2fd679ffc81f37cbccbdc953556ad0a492f3ca4");
    assert!(!lo_migrate::utils::has_migration_been_completed(&conn));
}

#[test]
#[should_panic(
    expected = "Hash of unknown type encountered: \"b2fd679ffc81f37cbccbdc953556aD0a492f3ca4\""
)]
fn invalid_hash() {
    let conn = postgres_conn();
    create_table_with_hash(&conn, "b2fd679ffc81f37cbccbdc953556aD0a492f3ca4");
    lo_migrate::utils::has_migration_been_completed(&conn);
}

#[test]
#[should_panic(expected = "Hash of unknown type encountered: \"b2fd679ffc\"")]
fn short_hash() {
    let conn = postgres_conn();
    create_table_with_hash(&conn, "b2fd679ffc");
    lo_migrate::utils::has_migration_been_completed(&conn);
}

fn create_table_with_hash(conn: &postgres::Connection, hash: &str) {
    conn.batch_execute(
        r##"
                           CREATE TABLE _nice_binary (
                               hash VARCHAR(255) NOT NULL
                           );
        "##,
    )
    .unwrap();
    conn.execute(
        r##"
                           INSERT INTO
                               _nice_binary (hash)
                           VALUES
                               ($1);
                       "##,
        &[&hash],
    )
    .unwrap();
}
