#![allow(dead_code)]

extern crate postgres;
extern crate rand;
extern crate rusoto_core;
extern crate rusoto_credential;
extern crate rusoto_s3;

#[cfg(any(feature = "postgres_tests", feature = "s3_tests"))]
use self::rand::distributions::Alphanumeric;
use self::rand::Rng;
use self::rusoto_core::region::Region;
use self::rusoto_core::request::HttpClient;
use self::rusoto_credential::StaticProvider;
use self::rusoto_s3::{CreateBucketRequest, S3Client, S3};
use lo_migrate::thread::ThreadStat;
use std::env;

/// create connection to Postgres
pub fn postgres_conn() -> postgres::Connection {
    let db_name: String = rand::thread_rng()
        .sample_iter(&Alphanumeric)
        .take(63)
        .collect();
    let postgres_host = env::var("POSTGRES_HOST").unwrap_or_else(|_| "localhost".to_string());

    let create_conn = postgres::Connection::connect(
        format!("postgresql://postgres@{}/postgres", postgres_host),
        postgres::TlsMode::None,
    )
    .unwrap();
    create_conn
        .execute(&format!("CREATE DATABASE \"{}\"", db_name), &[])
        .unwrap();

    postgres::Connection::connect(
        format!("postgresql://postgres@{}/{}", postgres_host, db_name),
        postgres::TlsMode::None,
    )
    .unwrap()
}

/// create connection to S3
pub fn s3_conn() -> (S3Client, String) {
    let mut bucket_name: String = rand::thread_rng()
        .sample_iter(&Alphanumeric)
        .take(63)
        .collect();
    bucket_name.make_ascii_lowercase();
    let endpoint = env::var("S3_ENDPOINT").unwrap_or_else(|_| "http://localhost:8080".to_string());

    let region = Region::Custom {
        name: "eu-east-3".to_owned(),
        endpoint,
    };
    let credentials = StaticProvider::new_minimal("access_key".to_owned(), "secret_key".to_owned());
    let s3_client = S3Client::new_with(HttpClient::new().unwrap(), credentials, region);

    let req = CreateBucketRequest {
        bucket: bucket_name.clone(),
        ..Default::default()
    };
    let mut rt = tokio::runtime::Runtime::new().unwrap();
    rt.block_on(s3_client.create_bucket(req)).unwrap();

    (s3_client, bucket_name)
}

pub fn extract_object_stats(
    stats: &ThreadStat,
) -> (Option<u64>, Option<u64>, u64, u64, u64, u64, u64) {
    (
        stats.lo_total(),
        stats.lo_remaining(),
        stats.lo_observed(),
        stats.lo_received(),
        stats.lo_stored(),
        stats.lo_committed(),
        stats.lo_failed(),
    )
}

pub fn extract_byte_stats(stats: &ThreadStat) -> (Option<u64>, Option<u64>, u64, u64, u64) {
    (
        stats.bytes_total(),
        stats.bytes_remaining(),
        stats.bytes_received(),
        stats.bytes_stored(),
        stats.bytes_committed(),
    )
}
