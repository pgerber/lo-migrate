create TABLE nice_history_domain_entity (
    "xmlContent" character varying(255) NOT NULL
);

INSERT INTO _nice_binary (hash, size, mime_type, data, sha2)
VALUES
    ('d54b8600c18692bb4edd5f811e5dfb6e134bdfbb',  125, 'octet/stream', 198485882, null),
    ('5e1acda10709141e3b731db8bdf0409d2e9ad3c4',    0, 'octet/stream', 198485883, null);

INSERT INTO nice_history_domain_entity
VALUES ('d54b8600c18692bb4edd5f811e5dfb6e134bdfbb'), ('5e1acda10709141e3b731db8bdf0409d2e9ad3c4');
