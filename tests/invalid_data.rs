#![cfg(feature = "postgres_tests")]

extern crate env_logger;
extern crate lo_migrate;
extern crate log;
extern crate postgres;
extern crate sha2;
extern crate two_lock_queue as queue;

mod common;
use crate::common::*;

use lo_migrate::thread::{Counter, Observer, Receiver, ThreadStat};
use sha2::Sha256;
use std::sync::Arc;

/// Test complete migration from Postgres to S3
#[test]
fn invalid_data() {
    let _ = env_logger::try_init();

    let stats = ThreadStat::new();
    let pg_conn = postgres_conn();

    // create database
    pg_conn
        .batch_execute(include_str!("invalid_data.sql"))
        .unwrap();

    // count large objects
    let counter = Counter::new(&stats, &pg_conn);
    counter.start_worker().unwrap();
    assert_eq!(
        extract_object_stats(&stats),
        (Some(5), Some(5), 0, 0, 0, 0, 0)
    );
    assert_eq!(extract_byte_stats(&stats), (Some(422), Some(422), 0, 0, 0));

    // get list of large objects
    let (rcv_tx, rcv_rx) = queue::unbounded();
    let observer = Observer::new(&stats, &pg_conn);
    observer.start_worker(Arc::new(rcv_tx), 1024).unwrap();
    assert_eq!(
        extract_object_stats(&stats),
        (Some(5), Some(5), 3, 0, 0, 0, 2)
    );
    assert_eq!(extract_byte_stats(&stats), (Some(422), Some(422), 0, 0, 0));

    // fetch large objects from postgres
    let (str_tx, _str_rx) = queue::unbounded();
    let receiver = Receiver::new(&stats, &pg_conn);
    receiver
        .start_worker::<Sha256>(Arc::new(rcv_rx), Arc::new(str_tx), 28)
        .unwrap();
    assert_eq!(
        extract_object_stats(&stats),
        (Some(5), Some(5), 3, 0, 0, 0, 5)
    );
    assert_eq!(extract_byte_stats(&stats), (Some(422), Some(422), 84, 0, 0));
}
