#![cfg(feature = "postgres_tests")]
#![cfg(feature = "s3_tests")]

extern crate env_logger;
extern crate futures;
extern crate lo_migrate;
extern crate log;
extern crate postgres;
extern crate rusoto_core;
extern crate rusoto_credential;
extern crate rusoto_s3;
extern crate rustc_serialize as serialize;
extern crate sha2;
extern crate two_lock_queue as queue;

mod common;
use crate::common::*;

use lo_migrate::thread::{Committer, Counter, Observer, Receiver, Storer, ThreadStat};
use rusoto_s3::{GetObjectRequest, S3Client, S3};
use serialize::hex::ToHex;
use sha2::{Digest, Sha256};
use std::io::Read;
use std::sync::Arc;

// sha256 hashes of clean_data.sql sorted by OID (DB column data)
const SHA256_HEX: [&str; 5] = [
    "3307cbe8e7c3e16a821c4656a0d6499c7177dd9b269f48e12f376a1fb41d9590",
    "e86ae2fd5056fcd75f6a9ed884fdc23d880be6d5de6159901e564ed19b12aa00",
    "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855",
    "e97a63c34bb2299e977ec5aea161f49ffdd3c6a719c8838504e20f8a8db85ae2",
    "2bf30d0b24a4ac96b21da2bf401f2e16063839cec8263e7f3397b6fafbac02ad",
];

// mime types of clean_data.sql sorted by OID (DB column data)
const MIME_TYPES: [&str; 5] = [
    "",
    "octet/stream",
    "octet/stream",
    "text/plain",
    "octet/stream",
];

#[test]
fn migration_without_history() {
    let pg_conn = create_database();
    migration_helper(pg_conn);
}

#[test]
fn migration_with_history() {
    let _ = env_logger::try_init();
    let pg_conn = create_database();
    pg_conn
        .batch_execute(include_str!("history_data.sql"))
        .unwrap();
    migration_helper(pg_conn);
}

fn create_database() -> postgres::Connection {
    let _ = env_logger::try_init();
    let pg_conn = postgres_conn();
    pg_conn
        .batch_execute(include_str!("clean_data.sql"))
        .unwrap();
    pg_conn
}

/// Test complete migration from Postgres to S3
fn migration_helper(pg_conn: postgres::Connection) {
    let stats = ThreadStat::new();
    let (s3_client, bucket_name) = s3_conn();

    // count large objects
    let counter = Counter::new(&stats, &pg_conn);
    counter.start_worker().unwrap();
    // 7 and 8 include two invalid hashes
    assert_eq!(
        extract_object_stats(&stats),
        (Some(6), Some(5), 0, 0, 0, 0, 0)
    );
    assert_eq!(
        extract_byte_stats(&stats),
        (Some(10486022), Some(10485978), 0, 0, 0)
    );

    // get list of large objects
    let (rcv_tx, rcv_rx) = queue::unbounded();
    let observer = Observer::new(&stats, &pg_conn);
    observer.start_worker(Arc::new(rcv_tx), 1024).unwrap();
    assert_eq!(
        extract_object_stats(&stats),
        (Some(6), Some(5), 5, 0, 0, 0, 0)
    );
    assert_eq!(
        extract_byte_stats(&stats),
        (Some(10486022), Some(10485978), 0, 0, 0)
    );

    // fetch large objects from postgres
    let (str_tx, str_rx) = queue::unbounded();
    let receiver = Receiver::new(&stats, &pg_conn);
    receiver
        .start_worker::<Sha256>(Arc::new(rcv_rx), Arc::new(str_tx), 28)
        .unwrap();
    assert_eq!(
        extract_object_stats(&stats),
        (Some(6), Some(5), 5, 5, 0, 0, 0)
    );
    assert_eq!(
        extract_byte_stats(&stats),
        (Some(10486022), Some(10485978), 10485978, 0, 0)
    );

    // store objects to S3
    let (cmt_tx, cmt_rx) = queue::unbounded();
    let storer = Storer::new(&stats, 5 * 1024 * 1024 + 3);
    storer
        .start_worker(Arc::new(str_rx), Arc::new(cmt_tx), &s3_client, &bucket_name)
        .unwrap();
    assert_eq!(
        extract_object_stats(&stats),
        (Some(6), Some(5), 5, 5, 5, 0, 0)
    );
    assert_eq!(
        extract_byte_stats(&stats),
        (Some(10486022), Some(10485978), 10485978, 10485978, 0)
    );

    // commit sha256 hashes to postgres
    let committer = Committer::new(&stats, &pg_conn);
    committer.start_worker(Arc::new(cmt_rx), 2).unwrap();
    assert_eq!(
        extract_object_stats(&stats),
        (Some(6), Some(5), 5, 5, 5, 5, 0)
    );
    assert_eq!(
        extract_byte_stats(&stats),
        (Some(10486022), Some(10485978), 10485978, 10485978, 10485978)
    );

    // verify sha256 hashes
    let sha2_hashes: Vec<String> = pg_conn
        .query(
            r#"
                SELECT sha2 FROM _nice_binary
                WHERE
                    sha2 <> '0000000000000000000000000000000000000000000000000000000000000000'
                    AND sha2 <> 'beefbeefbeefbeefbeefbeefbeefbeefbeefbeefbeefbeefbeef012345678900'
                    AND sha2 IS NOT NULL
                ORDER BY data
            "#,
            &[],
        )
        .unwrap()
        .iter()
        .map(|row| row.get(0))
        .collect();
    assert_eq!(&sha2_hashes, &SHA256_HEX);

    // verify S3 bucket content
    for (sha256, mime_type) in SHA256_HEX.iter().zip(&MIME_TYPES) {
        assert_object_in_store(&s3_client, &bucket_name, sha256, mime_type);
    }
}

fn assert_object_in_store(client: &S3Client, bucket_name: &str, expected_sha256: &str, mime: &str) {
    let request = GetObjectRequest {
        bucket: bucket_name.to_string(),
        key: expected_sha256.to_string(),
        ..Default::default()
    };
    let mut rt = tokio::runtime::Builder::new()
        .threaded_scheduler()
        .enable_all()
        .core_threads(2)
        .build()
        .unwrap();
    let response = rt.block_on(client.get_object(request)).unwrap();
    let mut actual_sha256 = Sha256::default();
    let mut body = Vec::new();
    response
        .body
        .unwrap()
        .into_blocking_read()
        .read_to_end(&mut body)
        .unwrap();
    actual_sha256.input(&body);

    assert_eq!(expected_sha256, &actual_sha256.result().to_hex());
    assert_eq!(&response.content_type.unwrap(), mime);
}
