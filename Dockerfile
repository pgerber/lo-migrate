FROM rust:1-stretch
WORKDIR /usr/src/lo_migrate
COPY ./src ./src
COPY Cargo.lock Cargo.toml ./
RUN cargo install --path /usr/src/lo_migrate

ENTRYPOINT ["lo_migrate"]
